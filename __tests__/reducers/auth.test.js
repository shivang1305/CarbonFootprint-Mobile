// import AuthReducer from '../../app/reducers/auth';
// import {
//     REQUEST_AUTH,
//     RECEIVE_AUTH,
//     RECEIVE_ERROR,
// } from '../../app/actions/AuthAction';

describe('testing auth reducer suite', () => {
    const EXPECTED_INITIAL_STATE = {
        isFetching: false,
        user: {},
        error: ''
    };

    const UNEXPECTED_INITIAL_STATE = {
        isFetching: false,
        user: undefined,
        error: ''
    };

    test('initial/default state', () => {
        // switching over undefined
        // let actualState = AuthReducer(undefined, {});
        // expect(actualState).not.toEqual(EXPECTED_INITIAL_STATE);
        // expect(actualState).toEqual(UNEXPECTED_INITIAL_STATE);
        // actualState = AuthReducer(undefined, { type: 'arbitrary123' });
        // expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
        // expect(actualState).not.toEqual(UNEXPECTED_INITIAL_STATE);
    });

    // test('REQUEST_AUTH type passed', () => {
    //     let actualState = AuthReducer(undefined, { type: 'REQUEST_AUTH' });

    //     let expectedState = {
    //         isFetching: true,
    //         user: {},
    //         error: ''
    //     };
    //     expect(actualState).toEqual(expectedState);
    // });

    // test('RECIEVE_AUTH type passed', () => {
    //     let actualState = AuthReducer(undefined, { type: 'RECEIVE_AUTH', user: 'jonas' });

    //     let expectedState = {
    //         isFetching: false,
    //         user: 'jonas',
    //         error: ''
    //     };
    //     // expect(actualState).not.toEqual(EXPECTED_INITIAL_STATE)
    //     expect(actualState).toEqual(expectedState);
    //     expect(actualState).toEqual({ ...EXPECTED_INITIAL_STATE, user: 'jonas' });
    // });

    // test('RECEIVE_ERROR type passed', () => {
    //     let actualState = AuthReducer(undefined, {
    //         type: 'RECEIVE_ERROR',
    //         error: { message: 'This is an error' }
    //     });

    //     let expectedState = {
    //         isFetching: false,
    //         user: null,
    //         error: 'This is an error'
    //     };
    //     // expect(actualState).not.toEqual(EXPECTED_INITIAL_STATE)
    //     expect(actualState).toEqual(expectedState);
    //     expect(actualState).toEqual({
    //         ...EXPECTED_INITIAL_STATE,
    //         error: 'This is an error',
    //         user: null
    //     });
    // });
});
