import Reducer from '../../app/reducers/storage';
import { REQUEST_STORAGE, RECEIVE_STORAGE } from '../../app/actions/StorageAction';

const stateMerge = (EXPECTED_INITIAL_STATE = {}, obj = {}, additionalProperties = {}) => {
    let mergedObject = {
        ...EXPECTED_INITIAL_STATE,
        ...obj,
        ...additionalProperties
    };

    // can also use "key" in mergedObject -> it will go down the proptotype chain though.
    if (mergedObject.hasOwnProperty('type')) delete mergedObject.type;

    return mergedObject;
};

describe('Intro test suite case', () => {
    const EXPECTED_INITIAL_STATE = {
        isFetching: false,
        data: {
            automobile: 'Car',
            type: 'Petrol',
            value: '10.3',
            unit: 'km/litre'
        }
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = Reducer(undefined, {});

        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('REQUEST_STORAGE type passed', () => {
        let action = {
            type: REQUEST_STORAGE
        };
        let actualState = Reducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: true });

        expect(actualState).toEqual(expectedState);
    });

    test('RECEIVE_STORAGE type passed', () => {
        let action = {
            type: RECEIVE_STORAGE,
            data: 'some data'
        };
        let actualState = Reducer(undefined, action);

        let expectedState = stateMerge(EXPECTED_INITIAL_STATE, action, { isFetching: false });

        expect(actualState).toEqual(expectedState);
    });
});
