import ActivityReducer from '../../app/reducers/activity';
import {
    SET_ACTIVITY_DATE,
    SET_ACTIVITY_START_TIME,
    SET_ACTIVITY_DURATION,
    SET_ACTIVITY_SRC,
    SET_ACTIVITY_DEST,
    SET_ACTIVITY_TYPE,
    SET_ACTIVITY_DISTANCE,
    SET_ACTIVITY_CO2
} from '../../app/actions/ActivityDetailsAction';
import { formatAMPM } from '../../app/config/helperWithoutStore';

describe('testing activity reducer suite', () => {
    function mutateExpectedInitialState(key, value) {
        return {
            ...EXPECTED_INITIAL_STATE,
            [key]: value
        };
    }

    const EXPECTED_INITIAL_STATE = {
        date: new Date().toDateString(),
        startTime: formatAMPM(new Date()),
        duration: 0,
        src: { latitude: -1, longitude: -1 },
        dest: { latitude: -1, longitude: -1 },
        type: 'STILL',
        distance: 0,
        co2: 0
    };

    test('initial/default state', () => {
        // switching over undefined
        let actualState = ActivityReducer(undefined, {});

        expect(actualState).toEqual(EXPECTED_INITIAL_STATE);
    });

    test('1. SET_ACTIVITY_DATE type passed', () => {
        let action = {
            type: SET_ACTIVITY_DATE,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('date', action.value);

        expect(actualState).toEqual(expectedState);
    });

    test('2. SET_ACTIVITY_START_TIME type passed', () => {
        let action = {
            type: SET_ACTIVITY_START_TIME,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('startTime', action.value);
        expect(actualState).toEqual(expectedState);
    });

    test('3. SET_ACTIVITY_DURATION type passed', () => {
        let action = {
            type: SET_ACTIVITY_DURATION,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('duration', action.value);
        expect(actualState).toEqual(expectedState);
    });

    test('4. SET_ACTIVITY_SRC type passed', () => {
        let action = {
            type: SET_ACTIVITY_SRC,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('src', action.value);
        expect(actualState).toEqual(expectedState);
    });

    test('5. SET_ACTIVITY_DEST type passed', () => {
        let action = {
            type: SET_ACTIVITY_DEST,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('dest', action.value);
        expect(actualState).toEqual(expectedState);
    });

    test('6. SET_ACTIVITY_TYPE type passed', () => {
        let action = {
            type: SET_ACTIVITY_TYPE,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('type', action.value);
        expect(actualState).toEqual(expectedState);
    });

    test('7. SET_ACTIVITY_DISTANCE type passed', () => {
        let action = {
            type: SET_ACTIVITY_DISTANCE,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('distance', action.value);
        expect(actualState).toEqual(expectedState);
    });

    test('8. SET_ACTIVITY_CO2 type passed', () => {
        let action = {
            type: SET_ACTIVITY_CO2,
            value: 'something'
        };
        let actualState = ActivityReducer(undefined, action);

        let expectedState = mutateExpectedInitialState('co2', action.value);
        expect(actualState).toEqual(expectedState);
    });
});
