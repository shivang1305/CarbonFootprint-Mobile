import * as AuthAction from '../../../app/actions/AuthAction';
import {
    stateMerge,
    storeFactory,
    initAdmin,
    initFirebase,
    wipeDatabase
} from '../../../app/config/testUtil';
import { registerFirebase, loginEmailFirebase } from '../../../app/actions/firebase/Auth';

jest.setTimeout(100000);

describe('AuthAction test suite case', () => {
    let store = null;
    const REDUCER_KEY = 'auth';
    const INITIAL_STATE = storeFactory().getState()[REDUCER_KEY];
    let testUser = null;

    beforeAll(() => {
        initFirebase();
        initAdmin();
    });
    beforeEach(async () => {
        store = storeFactory();
        wipeDatabase();
    });

    test('REQUEST_AUTH dispatch', () => {
        store.dispatch(AuthAction.requestAuth());
        let expectedState = {
            ...INITIAL_STATE,
            isFetching: true
        };
        let actualState = store.getState()[REDUCER_KEY];
        expect(actualState).toEqual(expectedState);
    });

    test('RECEIVE_AUTH dispatch', () => {
        let user = 'something';
        store.dispatch(AuthAction.receiveAuth(user));
        let expectedState = {
            ...INITIAL_STATE,
            user,
            isFetching: false
        };
        let actualState = store.getState()[REDUCER_KEY];
        expect(actualState).toEqual(expectedState);
    });

    test('RECEIVE_ERROR dispatch', () => {
        let error = {
            message: 'something'
        };
        store.dispatch(AuthAction.receiveError(error));
        let expectedState = {
            ...INITIAL_STATE,
            error: error.message,
            user: null,
            isFetching: false
        };
        let actualState = store.getState()[REDUCER_KEY];
        expect(actualState).toEqual(expectedState);
    });

    // test('login dispatch', async () => {
    //     let expectedState = {
    //         ...INITIAL_STATE,
    //         user: 'eef'
    //     }

    //     loginEmailFirebase = jest.fn(() => {
    //         return new Promise((res,rej) => {
    //             res('testUser')
    //         })
    //     })

    //     await store.dispatch(AuthAction.login('testUser@test.com','testPass'))

    //     let actualState = store.getState()[REDUCER_KEY]
    //     expect(actualState).toEqual(expectedState)

    // })
});
