import InviteTab from '../../app/containers/InviteTab';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('InviteTab test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(InviteTab, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'invitetab-container');

        expect(rootComponent.length).toBe(1);
    });
});
