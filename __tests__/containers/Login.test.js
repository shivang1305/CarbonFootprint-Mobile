import Login from '../../app/containers/Login';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Home test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Login, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'login-container');

        expect(rootComponent.length).toBe(1);
    });
});
