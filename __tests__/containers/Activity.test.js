import Activity from '../../app/containers/Activity';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Activity test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Activity, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'activity-container');

        expect(rootComponent.length).toBe(1);
    });
});
