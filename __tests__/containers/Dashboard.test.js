import Dashboard from '../../app/containers/Dashboard';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Dashboard test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Dashboard, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'dashboard-container');

        expect(rootComponent.length).toBe(1);
    });
});
