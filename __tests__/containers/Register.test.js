import Register from '../../app/containers/Register';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Register test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Register, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'register-container');

        expect(rootComponent.length).toBe(1);
    });
});
