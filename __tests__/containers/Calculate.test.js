import Calculate from '../../app/containers/Calculate';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('Calculate test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(Calculate, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'calculate-container');

        expect(rootComponent.length).toBe(1);
    });
});
