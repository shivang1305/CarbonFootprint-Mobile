import ProfileModal from '../../app/containers/ProfileModal';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('ProfileModal test suite', () => {
    let wrapper = null;

    beforeEach(() => {
        wrapper = setup(ProfileModal, undefined, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'profilemodal-container');

        expect(rootComponent.length).toBe(1);
    });
});
