import EmailSent from '../../app/components/EmailSent';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('EmailSent test suite case', () => {
    test('renders without error', () => {
        let expectedProps = {
            email: 'testing101@kentcdodds.com'
        };

        const wrapper = setup(EmailSent, expectedProps);
        let rootComponent = findByAttr(wrapper, 'emailsent-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Checking proptypes', () => {
        const requiredProps = {
            email: 'testing101@kentcdodds.com'
        };
        const propError = checkProps(EmailSent, requiredProps);
        // check-prop-types returns an error if required
        //props are not sent. Else undefined is returned
        // which is equivalent to no return statement.
        expect(propError).toBeUndefined();
    });
});
