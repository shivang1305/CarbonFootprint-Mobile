import React from 'react';
import About from '../../app/components/About';
import { setup, findByAttr } from '../../app/config/testUtil';

// UNIT TESTING
// 1. Testing actions.
// 2. Testing reducers.
// 3. Testing components & containers.
// Components is a good start, for it is not connected to redux.
// INTEGRATION TESTING
// 1. Testing updating of redux global state from actions dispatching to reducers to state change.

test('renders without error', () => {
    const wrapper = setup(About);
    let rootComponent = findByAttr(wrapper, 'about-component');
    let socialContainer = findByAttr(wrapper, 'social-links');

    // always passes because does not throw an error.
    // Just returns empty object if not present element.
    expect(rootComponent).toBeTruthy();
    //no. of components with testID = about-component
    expect(rootComponent.length).toBe(1);

    // no. of child nodes.
    expect(socialContainer.children()).toHaveLength(4);
});
