import React from 'react';
import { shallow, mount } from 'enzyme';
import { TouchableHighlight } from 'react-native';
import Footer from '../../app/components/Footer';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

test('renders without error', () => {
    // TODO: mount instead of shallow
    // mounting component onto dom, simulates all the
    // lifecycle hooks that are called
    // const wrapper = mount(<Footer/>)
    const wrapper = setup(Footer);
    // another shallow wrapper
    let rootComponent = findByAttr(wrapper, 'footer-component');

    expect(rootComponent).toBeTruthy();
    expect(rootComponent.length).toBe(1);
});

test('renders 5 tabs(icons) in footer', () => {
    const wrapper = setup(Footer);
    let tabs = findByAttr(wrapper, 'tab');

    // five footer navigational icons
    expect(tabs).toHaveLength(5);
});

test('Checking proptypes', () => {
    // There is no required prop for Footer
    let propError = checkProps(Footer, {});
    expect(propError).toBeUndefined();
});
