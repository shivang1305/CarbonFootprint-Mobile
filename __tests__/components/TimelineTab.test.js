import TimelineTab from '../../app/components/TimelineTab';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';
import ActivityHistoryStorage from '../../app/actions/ActivityHistoryStorage';

describe('TimelineTab test suite', () => {
    let wrapper = null;
    ActivityHistoryStorage.createDB = jest.fn(() => console.log('createDB called'));
    ActivityHistoryStorage.getData = jest.fn(() => {
        return [
            {
                src: 'Mock Source',
                startTime: 'Start time',
                actType: 'some type',
                distance: 3.44,
                duration: 10,
                co2Emitted: 433,
                co2Saved: 542
            }
        ];
    });

    beforeEach(() => {
        wrapper = setup(TimelineTab);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'timelinetab-component');
        let timeline = findByAttr(wrapper, 'timeline');
        let noactivityText = findByAttr(wrapper, 'noactivity-text');

        expect(rootComponent.length).toBe(1);
        expect(timeline.length).toBe(1);
        expect(noactivityText.length).toBe(0);
    });
});
