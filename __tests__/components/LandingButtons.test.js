import LandingButtons from '../../app/components/LandingButtons';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('LandingButtons test suite case', () => {
    let expectedProps = {
        fbLogin: jest.fn(),
        googleSignIn: jest.fn()
    };
    let wrapper = null;

    beforeEach(() => {
        expectedProps.fbLogin.mockClear();
        expectedProps.googleSignIn.mockClear();
        wrapper = setup(LandingButtons, expectedProps, undefined, true);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'landingbuttons-component');

        expect(rootComponent.length).toBe(1);
    });

    test('simulate button press', () => {
        let facebookButton = findByAttr(wrapper, 'buttons').at(0);
        let googleButton = findByAttr(wrapper, 'buttons').at(1);

        let clickTimesFacebookButton = Array.from(Array(3));
        let clickTimesGoogleButton = Array.from(Array(5));

        clickTimesFacebookButton.forEach(() => facebookButton.simulate('press'));
        clickTimesGoogleButton.forEach(() => googleButton.simulate('press'));

        expect(facebookButton.length).toBe(1);
        expect(googleButton.length).toBe(1);
        expect(expectedProps.fbLogin.mock.calls).toHaveLength(3);
        expect(expectedProps.googleSignIn.mock.calls).toHaveLength(5);
    });

    test('Checking proptypes', () => {
        const propError = checkProps(LandingButtons, expectedProps);

        expect(propError).toBeUndefined();
    });
});
