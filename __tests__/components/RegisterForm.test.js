import RegisterForm from '../../app/components/RegisterForm';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('RegisterForm test suite', () => {
    let wrapper = null;
    let expectedProps = {
        auth: {},
        register: jest.fn()
    };

    beforeEach(() => {
        wrapper = setup(RegisterForm, expectedProps);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'registerform-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Check prop types', () => {
        let requiredProps = {
            register: jest.fn()
        };

        let propError = checkProps(RegisterForm, expectedProps);

        // propError is undefined if required props are passed
        expect(propError).toBeUndefined();
    });
});
