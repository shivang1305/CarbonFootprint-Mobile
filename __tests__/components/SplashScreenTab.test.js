import SplashScreenTab from '../../app/components/SplashScreenTab';
import { shallow } from 'enzyme';
import React from 'react';
import checkPropTypes from 'check-prop-types';

const setup = (Component, props = {}, initialState = null) => {
    let wrapper = shallow(<Component {...props} />);
    if (initialState) wrapper.setState(state);

    return wrapper;
};

/**
 * Factory function to return the ShallowWrapper with the root as node having testID = val.
 * @param {ShallowWrapper} wrapper - Given ShallowWrapper to search within.
 * @param {string} val - Value of testID attribute for search.
 * @return {ShallowWrapper} - Return ShallowWrapper containing node(s) with the given testID attribute
 */
const findByAttr = (wrapper, val) => {
    return wrapper.find(`[testID="${val}"]`);
};

/**
 * Checks if the required props are being passed to a component.
 * @param {React.Component} component
 * @param {object} conformingProps
 */
const checkProps = (component, conformingProps) => {
    const propError = checkPropTypes(component.propTypes, conformingProps, 'prop', component.name);
    return propError;
};

describe('SplashScreenTab test suite', () => {
    test('renders without error', () => {
        let expectedProps = {
            tab: {}
        };
        const wrapper = setup(SplashScreenTab, expectedProps);
        let rootComponent = findByAttr(wrapper, 'splashscreentab-component');
        expect(rootComponent.length).toBe(1);
    });

    test('Checking prop types', () => {
        let expectedProps = {
            tabs: {}
        };
        const propError = checkProps(expectedProps);
        expect(propError).toBeUndefined();
    });
});
