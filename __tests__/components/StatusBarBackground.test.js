import { Platform } from 'react-native';
import StatusBarBackground, {
    isIPhoneXSize,
    isIPhoneXrSize,
    returnHeightOfStatusBar
} from '../../app/components/StatusBarBackground';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('FriendsTabBar test suite', () => {
    test('renders without error', () => {
        const wrapper = setup(StatusBarBackground);
        let rootComponent = findByAttr(wrapper, 'statusbarbackground-component');

        expect(rootComponent.length).toBe(1);
    });

    test('Helper functions', () => {
        let dim = {
            width: 812,
            height: 896
        };
        let iPhoneX = isIPhoneXSize(dim);
        let iPhoneXr = isIPhoneXrSize(dim);

        Platform.OS = 'android';
        const androidStatusBarHeight = returnHeightOfStatusBar();

        Platform.OS = 'ios';
        // Because of no simulator, 0 width/height expected
        const iosStatusBarHeight = returnHeightOfStatusBar();

        expect(androidStatusBarHeight).toBe(0);
        expect(iosStatusBarHeight).toBe(18);
        expect(iPhoneX).toBeTruthy();
        expect(iPhoneXr).toBeTruthy();
    });
});
