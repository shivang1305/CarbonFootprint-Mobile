import ActivityTab from '../../app/components/ActivityTab';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('ActivityTab test suite', () => {
    let wrapper = null;
    let expectedProps = {
        activity: {
            distance: 9.45
        },
        setSrc: jest.fn(),
        setCO2: jest.fn(),
        setDest: jest.fn(),
        startActivityDetection: jest.fn()
    };
    ActivityTab.prototype.componentWillMount = jest.fn();

    beforeEach(() => {
        wrapper = setup(ActivityTab, expectedProps);
    });

    test('renders without error', () => {
        let rootComponent = findByAttr(wrapper, 'activitytab-component');

        expect(rootComponent.length).toBe(1);
    });

    // test('simulate clicking on tab', () => {
    //     let expectedProps = {
    //         onChangeTab: jest.fn(val => val)
    //     }

    //     const wrapper = setup(FootprintCard, expectedProps)
    //     let rootComponent = findByAttr(wrapper,"footprintcard-component")
    //     let firstTabComponent = findByAttr(rootComponent, "tab").first()

    //     firstTabComponent.simulate('press')
    //     // Called once
    //     expect(expectedProps.onChangeTab.mock.calls).toHaveLength(1)
    //     // first argument value, called with val = 0
    //     expect(expectedProps.onChangeTab.mock.calls[0][0]).toBe(0)
    //     // return value
    //     expect(expectedProps.onChangeTab.mock.results[0].value).toBe(0)

    // })

    test('Check prop types', () => {
        let propError = checkProps(ActivityTab, expectedProps);
        expect(propError).toBeUndefined();
    });
});
