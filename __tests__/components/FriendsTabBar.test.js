import FriendsTabBar from '../../app/components/FriendsTabBar';
import { setup, findByAttr, checkProps } from '../../app/config/testUtil';

describe('FriendsTabBar test suite', () => {
    test('renders without error', () => {
        let expectedProps = {
            tabs: []
        };
        const wrapper = setup(FriendsTabBar, expectedProps);
        let rootComponent = findByAttr(wrapper, 'friendstabbar-component');
        expect(rootComponent.length).toBe(1);
    });

    test('Checking prop types', () => {
        let expectedProps = {
            tabs: []
        };
        const propError = checkProps(expectedProps);
        expect(propError).toBeUndefined();
    });
});
