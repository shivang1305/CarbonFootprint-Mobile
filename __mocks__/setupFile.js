import { NativeModules } from 'react-native';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new EnzymeAdapter() });

jest.mock('react-native-google-signin', () => {
    const GoogleSigninButton = () => {};
    GoogleSigninButton.Color = {
        Auto: 0,
        Light: 1,
        Dark: 2
    };
    GoogleSigninButton.Size = {
        Icon: 0,
        Standard: 1,
        Wide: 2
    };

    return GoogleSigninButton;
});

jest.mock('react-native-simple-toast', () => ({
    SHORT: jest.fn()
}));

jest.mock('react-native-activity-recognition', () => {
    return {};
});

jest.mock('react-native-background-timer', () => {
    return {};
});

jest.mock('react-native-firebase', () => {
    return {
        messaging: jest.fn(() => {
            return {
                hasPermission: jest.fn(() => Promise.resolve(true)),
                subscribeToTopic: jest.fn(),
                unsubscribeFromTopic: jest.fn(),
                requestPermission: jest.fn(() => Promise.resolve(true)),
                getToken: jest.fn(() => Promise.resolve('myMockToken'))
            };
        }),
        notifications: jest.fn(() => {
            return {
                onNotification: jest.fn(),
                onNotificationDisplayed: jest.fn()
            };
        })
    };
});

jest.mock('react-native-fetch-blob', () => {
    return {};
});

NativeModules.RNGoogleSignin = {
    SIGN_IN_CANCELLED: '0',
    IN_PROGRESS: '1',
    PLAY_SERVICES_NOT_AVAILABLE: '2',
    SIGN_IN_REQUIRED: '3'
};

export { NativeModules };
